# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui.ui'
#
# Created: Tue May 21 17:59:35 2013
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyTextEdit import PyTextEdit

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(800, 600)
        MainWindow.setWindowOpacity(1.0)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.statusLB = QtGui.QLabel(self.centralwidget)
        self.statusLB.setGeometry(QtCore.QRect(21, 577, 491, 16))
        self.statusLB.setObjectName(_fromUtf8("statusLB"))
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 540, 239, 25))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.openPB = QtGui.QPushButton(self.layoutWidget)
        self.openPB.setObjectName(_fromUtf8("openPB"))
        self.horizontalLayout.addWidget(self.openPB)
        self.closePB = QtGui.QPushButton(self.layoutWidget)
        self.closePB.setObjectName(_fromUtf8("closePB"))
        self.horizontalLayout.addWidget(self.closePB)
        self.clearPB = QtGui.QPushButton(self.layoutWidget)
        self.clearPB.setObjectName(_fromUtf8("clearPB"))
        self.horizontalLayout.addWidget(self.clearPB)
        self.dispTE = PyTextEdit(self.centralwidget)
        self.dispTE.setGeometry(QtCore.QRect(20, 20, 750, 500))
        self.dispTE.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.dispTE.setReadOnly(False)
        self.dispTE.setObjectName(_fromUtf8("dispTE"))
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "PySerial", None))
        self.statusLB.setText(_translate("MainWindow", "PySerial", None))
        self.openPB.setText(_translate("MainWindow", "打开", None))
        self.closePB.setText(_translate("MainWindow", "关闭", None))
        self.clearPB.setText(_translate("MainWindow", "清除", None))

